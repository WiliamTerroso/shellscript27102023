#!/bin/bash

# Verificar se o script está sendo executado como usuário ifpb
if [[ $user != "ifpb" || $(id -u) != 1000 ]]; then
  # Exibir uma mensagem de erro
  echo "O script deve ser executado como usuário ifpb com ID 1000."
  exit 1
fi

# Obter o diretório passado como argumento de linha de comando
if [[ $1 ]]; then
  # Criar um ponteiro para o diretório
  dir=$1
else
  # Usar o caminho /home/ifpb como padrão
  dir="/home/ifpb"
fi

# Contar a quantidade de arquivos
num_arquivos=$(find "$dir" -type f | wc -l)

# Contar a quantidade de diretórios
num_diretorios=$(find "$dir" -type d | wc -l)

# Exibir as informações solicitadas
echo "Número de arquivos: $num_arquivos"
echo "Número de diretórios: $num_diretorios"

